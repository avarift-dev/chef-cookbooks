# **Avarift Cookbook**
---

This master cookbook contains the following cookbooks used in the web interface for Avarift.com:

|     Cookbook    | Version | Chef Version |
| --------------- |:-------:|:------------:|
| apt             |  5.0.1  |      11+     |
| compat_resource | 12.16.3 |      11+     |
| homebrew        |  3.0.0  |      11+     |
| java            |  5.0.1  |      11+     |
| ohai            | 1.46.0  |      11+     |
| tomcat          |  2.3.4  |      11+     |
| windows         |  2.1.1  |      11+     |

## Reminder
---

To download cookbooks to Windows:
1.	Open up chefdk in powershell 
2.	`cd` to the cookbook directory 
3.	Edit the Berksfile to include or remove cookbooks 
4.	Run `berks vendor recipes` 
> This installs the cookbooks in the nested  **recipes** directory 

