name        "dnsupdate"
description "Update Route53 Zone"
maintainer  "Avarift"
license     "Apache 2.0"
version     "1.0.0"

depends "route53"